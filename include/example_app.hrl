%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 04.04.2021
%%% @doc

-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

%% ====================================================================
%% Define modules and variables
%% ====================================================================

%% Application
-define(APP, example_app).
-define(ServiceName, "ExampleApp"). % prefix for output in logs
-define(SUPV, example_app_supv).
-define(Srv, example_app_srv).

%% other apps
-define(APPBL, basiclib).

%% basiclib app
-define(BU, basiclib_utils).
-define(BWSupv, basiclib_worker_supv).
-define(BWSrv, basiclib_worker_srv).
-define(BRpc, basiclib_rpc).
-define(BLlog, basiclib_log).

%% Utils
-define(U, example_app_utils).

%% ====================================================================
%% Define Records
%% ====================================================================

-record(lsrvstate, {ref}).

%% ====================================================================
%% Define logs
%% ====================================================================

%% basiclib log
-define(LOGFILE, {example_app,trace}).

-define(LOG(Level,Fmt,Args), ?BLlog:write(Level,?LOGFILE,{Fmt,Args})).
-define(LOG(Level,Text), ?BLlog:write(Level,?LOGFILE,Text)).

-define(OUT(Text), ?BLlog:out(Text)).
-define(OUT(Level,Fmt,Args), ?BLlog:writeout(Level,?LOGFILE,{Fmt,Args})).
-define(OUT(Level,Text), ?BLlog:writeout(Level,?LOGFILE,Text)).

-define(OUTC(Level,Fmt,Args), ?BLlog:out(Fmt,Args)).
-define(OUTC(Level,Text), ?BLlog:out(Text)).

%% local
-define(OUTL(Fmt,Args), io:format("~s. "++Fmt++"~n",[?ServiceName]++Args)).
-define(OUTL(Text), io:format("~s. "++Text++"~n",[?ServiceName])).