%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 04.04.2021
%%% @doc

-module(example_app_srv).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-behaviour(gen_server).

-export([start_link/1,
         %
         init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3
        ]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("example_app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ---
start_link(MapOpts) ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, MapOpts, []).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(_MapOpts) ->
    Ref = erlang:make_ref(),
    State = #lsrvstate{ref=Ref},
    ?OUTL("Inited. Local: ~p (~p).", [?MODULE, self()]),
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------

%% default
handle_call(_Request, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

%% default
handle_cast(_Request, State) ->
    ?OUTL("~p. RCV unknown CAST:~p",[?ServiceName,_Request]),
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

%% default
handle_info(_Info, State) ->
    ?OUTL("~p. RCV unknown INFO:~p",[?ServiceName,_Info]),
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================
