%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 04.04.2021
%%% @doc

-module(example_app_utils).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-export([drop_last_subdir/2,
         ensure_dir/1]).

-export([check_connstr/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("example_app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---
-spec ensure_dir(DirPath::binary()) -> ok | {error,Reason::file:posix()}.
%% ---
ensure_dir(Path) when is_binary(Path) ->
    PathToCreate = <<Path/binary,"/">>,
    filelib:ensure_dir(PathToCreate).

%% ---
-spec drop_last_subdir(Path::string(),N::integer()) -> NPath::string().
%% ---
drop_last_subdir(Path, 0) -> Path;
drop_last_subdir(Path, N)
  when is_integer(N) ->
    drop_last_subdir(
      lists:droplast(
        lists:reverse(
          lists:dropwhile(fun($/) -> false; (_) -> true end, lists:reverse(Path)))), N-1).

%% ---
%% for debug
check_connstr(ConnStr) ->
    ?OUTL("check_connstr. ConnStr:~120p",[ConnStr]),
    ok.

%% ====================================================================
%% Internal functions
%% ====================================================================