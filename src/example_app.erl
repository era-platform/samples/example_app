%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 04.04.2021
%%% @doc

-module(example_app).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

-behaviour(application).

-export([start/0, start/2,
         stop/0,stop/1,
         %
         update_params/1,
         update_config/1
        ]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("example_app.hrl").

%% ====================================================================
%% Public API
%% ====================================================================

%% ---
-spec start() -> ok | {error,Reason::term()}.
%% ---
start() ->
    ?OUTL("Application start.", []),
    add_deps_paths(),
    setup_deps_env(),
    Result = application:ensure_all_started(?APP, permanent),
    ?OUTL("Application start. Result:~120p",[Result]),
    case Result of
        {ok, _Started} -> ok;
        {error,_}=Error -> Error
    end.

%% ---
-spec stop() -> ok | {error,Reason::term()}.
%% ---
stop() ->
    application:stop(?APP).

%% ---
-spec update_params(Opts::term()) -> ok.
%% ---
update_params(Opts) ->
    ?OUTL("Rcv update application params with opts:~120p",[Opts]),
    ok.

%% ---
-spec update_config(Opts::term()) -> ok.
%% ---
update_config(Opts) ->
    ?OUTL("Rcv update configuration with opts:~120p",[Opts]),
    ok.

%% ===================================================================
%% Private
%% ===================================================================

%% ---
start(_Mode, State) ->
    ?OUTL("Application start(Mode, State)", []),
    ?SUPV:start_link(State).

%% ---
stop(_State) ->
    ok.

%% ====================================================================
%% Internal
%% ====================================================================

%% ---
%% adds app's dependencies paths to code
add_deps_paths() ->
    Path = ?U:drop_last_subdir(code:which(?MODULE), 3),
    DepsPaths = filelib:wildcard(Path++"/*/ebin"),
    ?OUTL("DepsPaths:~120p", [DepsPaths]),
    AddPathsRes = code:add_pathsa(DepsPaths),
    ?OUTL("Add paths result:~120p", [AddPathsRes]),
    AddPathsRes.

%% ---
setup_deps_env() ->
    ?BU:set_env(?APPBL,max_loglevel,'$trace').